import java.io.*;
import java.io.FileReader;



public class Odczyt {
	public static void main (String []args) {
		try {
			File mojPlik = new File("test.txt");
			FileReader czytelnik = new FileReader(mojPlik);
			BufferedReader czytelnikB = new BufferedReader(czytelnik);
			
			String wiersz = null;
			
			while ((wiersz = czytelnikB.readLine()) != null) {
				System.out.println(wiersz);
			}
			czytelnikB.close();
		}catch(Exception e){
			e.printStackTrace();
			
		}
	}

}
