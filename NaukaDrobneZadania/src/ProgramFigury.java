
public class ProgramFigury {
	public static void main(String[] args) {
		System.out.println(3%3);
		Circle c = new Circle(5);
		Triangle t = new Triangle(5, 5);
		Rectangle r = new Rectangle(4, 5);
		Figure[] tabFigur = { c, t, r };

		for (Figure f : tabFigur) {
			System.out.println("Pole to: "+f.getArea());
			System.out.println("Obw�d to: "+f.getPerimeter());
		}
	}
}
