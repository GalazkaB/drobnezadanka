
public class Triangle implements Figure {
	double leg1;
	double leg2;
	String name = "tr�jk�ta";
	
	public Triangle(double eL1, double eL2) {
		leg1 = eL1;
		leg2 = eL2;
	}
	
	public double getPerimeter() {
		return leg1+leg2+ ( Math.sqrt(Math.pow(leg1, 2) + Math.pow(leg2, 2) ));
	}
	
	public double getArea() {
		return leg1*leg2*0.5;
	}
}
