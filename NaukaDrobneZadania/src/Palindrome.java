
public class Palindrome {

	public static boolean isPalindrome(String doSprawdzenia) {
		String reversed = new StringBuilder(doSprawdzenia).reverse().toString();
		boolean is = false;
		if (doSprawdzenia.equals(reversed)) {
			is = true;
		}
		return is;

	}
}
