
public class Rectangle implements Figure{
	double side1;
	double side2;
	String name = "prostokąta";
	
	public Rectangle(double s1, double s2) {
		side1 = s1;
		side2 = s2;
	}
	
	public double getPerimeter() {
		return side1*2+side2*2;
	}
	
	public double getArea() {
		return side1*side2;
	}
}
