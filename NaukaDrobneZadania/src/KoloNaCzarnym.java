import java.awt.Color;
import java.awt.Graphics;
import javax.swing.*;

public class KoloNaCzarnym extends JPanel {
	public void paintComponent(Graphics g) {
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		int czerwony = (int) (Math.random() * 256);
		int zielony = (int) (Math.random() * 256);
		int niebieski = (int) (Math.random() * 256);
		
		Color kolLos = new Color(czerwony, zielony, niebieski);
		g.setColor(kolLos);
		g.fillOval(70, 70, 100, 100);
	}
	
}
