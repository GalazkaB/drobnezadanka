
public class Circle implements Figure{
	double radius;
	String name = "ko�a";
	
	public Circle(double r) {
		radius=r;
	}
	
	public double getPerimeter() {
		return 2*Math.PI*radius;
	}
	
	public double getArea() {
		return Math.PI*Math.pow(radius, 2);
	}
}
