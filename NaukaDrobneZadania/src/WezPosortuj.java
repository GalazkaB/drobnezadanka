import java.util.Scanner;

public class WezPosortuj {
	static int ileWez;

	public static void main(String[] args) {
		for (int prob = 0;prob<10; prob++) {
		try {
			Pobierz p = new Pobierz();
			System.out.println("Podaj ilo�� liczb jak� chcesz zapisa�");
			Scanner s = new Scanner(System.in);
			String rozmiar = s.nextLine();
			ileWez = Integer.parseInt(rozmiar);
			p.pobrane = new int[ileWez];

			for (int i = 0; i < ileWez; i++) {
				System.out.println("Podaj liczb� dla pozycji: " + (i + 1));
				String odczytLiczb = s.nextLine();
				p.pobrane[i] = Integer.parseInt(odczytLiczb);
			}
			int[] posortowane = Sort.sortuj(p.pobrane);
			Pobierz.showTab(posortowane);
			break;
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Poda�e� liczb� mniejsz� ni� jeden go�ciu, spr�buj jeszcze raz! Pozosta�o Ci "+(9-prob)+" pr�b");
		}
	}
}
}
