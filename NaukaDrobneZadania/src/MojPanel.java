import java.awt.*;
import javax.swing.*;

public class MojPanel extends JPanel {

	public void paintComponent(Graphics g) {

		Graphics2D g2d = (Graphics2D) g;

		int czerwony = (int) (Math.random() * 256);
		int zielony = (int) (Math.random() * 256);
		int niebieski = (int) (Math.random() * 256);
		Color kolPocz = new Color(czerwony, zielony, niebieski);

		czerwony = (int) (Math.random() * 256);
		zielony = (int) (Math.random() * 256);
		niebieski = (int) (Math.random() * 256);
		Color kolKonc = new Color(czerwony, zielony, niebieski);

		GradientPaint grad = new GradientPaint(70, 70, kolPocz, 150, 150, kolKonc);

		g2d.setPaint(grad);
		g2d.fillOval(70, 70, 100, 100);

	}
}
