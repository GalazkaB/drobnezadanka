
public class SumaParzystychFibonacci {

	public static void main(String[] args) {
		int last1 = 0;
		int last2 = 1;
		int present = 2;
		int suma = 0;
		
		while (present < 4000000) {
			if (isEven(present)) {
				suma = suma + present;
			}
			last1 = last2;
			last2 = present;
			present = last2+last1; // 1,2,3,5,
			 
		}
		System.out.println(suma);
	}

	public static boolean isEven(int e) {
		boolean is = false;
		if (e % 2 == 0 && e != 0) {
			is = true;
		}
		return is;

	}
}
